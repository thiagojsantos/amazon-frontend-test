Feature: Amazon Test

  Scenario: Looking for Nikon D3X product on Amazon.com
    Given I open up "http://www.amazon.com" web page
    When I type Nikon on search bar
    And I click on search button
    And I sort results from highest price to slowest
    And select the second product
    Then the product topic contains text “Nikon D3X”