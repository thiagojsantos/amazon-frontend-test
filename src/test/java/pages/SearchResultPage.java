package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class SearchResultPage extends BasePage{

    // Page Elements
    private final By sortByDropdownSelector = By.cssSelector("#sort");
    private final By resultListSelector = By.cssSelector("#s-results-list-atf");

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public SearchResultPage clickOnSearchButton() {
        Select sortByDropdown = new Select(driver.findElement(sortByDropdownSelector));
        sortByDropdown.selectByValue("price-desc-rank");
        return new SearchResultPage(driver);
    }

    public SearchResultPage clickOnProductFromResult() {
        getElementWaiter(10).until(ExpectedConditions.elementToBeClickable(resultListSelector));
        List<WebElement> products = driver.findElements(resultListSelector);
        for (WebElement opt : products) {
            opt.findElement(By.cssSelector("#result_1 > div > div > div > div.a-fixed-left-grid-col.a-col-right > div.a-row.a-spacing-small > div:nth-child(1) > a")).click();
        }
        return new SearchResultPage(driver);
    }
}
