package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductDetailsPage extends BasePage {

    // Page Elements
    private final By productTopicSelector = By.cssSelector("#productTitle");

    public ProductDetailsPage(WebDriver driver) {
        super(driver);
    }

    public String getProductTopic(){
        getElementWaiter(10).until(ExpectedConditions.elementToBeClickable(productTopicSelector));
        WebElement productTopic = driver.findElement(productTopicSelector);
        return  productTopic.getText();
    }
}
