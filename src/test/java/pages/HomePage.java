package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

    // Page Elements
    private final By searchBarSelector = By.cssSelector("#twotabsearchtextbox");
    private final By searchButtonSelector = By.cssSelector("#nav-search > form > div.nav-right > div > input");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage navigateTo(String url) {
        driver.navigate().to(url);
        return new HomePage(driver);
    }

    public HomePage searchFor(String keyword) {
        WebElement searchBar = driver.findElement(searchBarSelector);
        searchBar.click();
        searchBar.sendKeys(keyword);
        return new HomePage(driver);
    }

    public HomePage clickOnSearchButton() {
        WebElement searchButton = driver.findElement(searchButtonSelector);
        searchButton.click();
        return new HomePage(driver);
    }
}
