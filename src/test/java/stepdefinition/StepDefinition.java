package stepdefinition;

import pages.HomePage;
import pages.ProductDetailsPage;
import pages.SearchResultPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class StepDefinition {
    WebDriver driver;

    @Before
    public void startUp() {
        driver = new FirefoxDriver();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Given("^I open up \\\"([^\\\"]*)\\\" web page$")
    public void i_open_up_page_web_page(String url) throws Throwable {
        HomePage homePage = new HomePage(driver);
        homePage.navigateTo(url);
    }

    @When("^I type (.*) on search bar$")
    public void i_type_on_search_bar(String keyword) throws Throwable {
        HomePage homePage = new HomePage(driver);
        homePage.searchFor(keyword);
    }

    @When("^I click on search button$")
    public void i_click_on_search_button() throws Throwable {
        HomePage homePage = new HomePage(driver);
        homePage.clickOnSearchButton();
    }

    @When("^I sort results from highest price to slowest$")
    public void i_sort_resuts_from_highest_price_to_slowest() throws Throwable {
        SearchResultPage searchResultPage = new SearchResultPage(driver);
        searchResultPage.clickOnSearchButton();
    }

    @When("^select the second product$")
    public void select_the_second_product() throws Throwable {
        SearchResultPage searchResultPage = new SearchResultPage(driver);
        searchResultPage.clickOnProductFromResult();
    }

    @Then("^the product topic contains text (.*)$")
    public void the_product_topic_contains_text(String topic) throws Throwable {
        ProductDetailsPage productDetailsPage = new ProductDetailsPage(driver);
        assertThat(productDetailsPage.getProductTopic(), containsString(topic));
    }
}
