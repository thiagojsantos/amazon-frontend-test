# Amazon FrontEnd Test

### About the project
  
Simple Test in which access Amazon website, looks for "Nikon D3X" on a product page.
 
### Project Dependencies

* Firefox browser version [33.11](https://ftp.mozilla.org/pub/firefox/releases/33.1.1/)
* Maven version 3.5.0
* Java version 1.8
* Selenium Webdriver 2.44.0

### Project Structure

               .
               +-- src
                  +-- test
                  |   +-- java
                  |   |  +-- page
                  |   |      +-- BasePage
                  |   |      +-- HomePage
                  |   |      +-- ProductDetailsPage
                  |   |      +-- SearchResultPage
                  |   |  +-- stepdefinition
                  |   |      +-- StepDefinition
                  |   |      +-- TestRunner
                  |   +-- resources
                         +-- amazon.feature
               +-- .gitignore
               +-- pom.xml
               +-- README.md
     
### Installation Instructions
 
* Project Installation
 
             $ git clone https://gitlab.com/thiagojsantos/amazon-frontend-test.git
             $ mvn install -DskipTests

Also make sure that Firefox version is compatible with Selenium Webdriver 2.44.0. For this project, Firefox version [33.11](https://ftp.mozilla.org/pub/firefox/releases/33.1.1/) was tested.


### Running Tests

            $ mvn test
            